/*********************************************************************/
//                                              ↓↓↓ モジュール読み込み↓↓↓
/*********************************************************************/
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var fs = require('fs');

//Expressのテンプレートについてくるので
//一応ロードする。
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//path.join()は引数に指定されたファイルを"/"で区切りパス情報にする
//faviconが決まったら、public直下に「favicon.ico」という名でおいてコメントを外す
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.set('views', path.join(__dirname, 'views'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());

//public直下のリソースを使用する際に、
//サーバー側、クライアント側の両方の記述が楽になる
app.use(express.static(path.join(__dirname, 'public')));

app.listen(3000, function() {
  console.log('Socket IO Server is listening on port 3000');
});


/*********************************************************************/
//                                             ↓↓↓ Error Handler↓↓↓
/*********************************************************************/
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

//------ production error handler-----
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


/*********************************************************************/
//                                             　↓↓↓ ページアクセス↓↓↓
/*********************************************************************/

app.get('/', function(req, res,next) { 
    res.sendFile(__dirname + '/views/client.html');
});


/*********************************************************************/
//                                             　↓↓↓ Socket.io↓↓↓
/*********************************************************************/

// 待ち受け
io.sockets.on('connection', function(socket) {
  console.log('connection...');
  socket.on('emit_from_client', function(data) {
    console.log('socket.io server received : '+data);
    // 接続しているソケット全部
    io.sockets.emit('emit_from_server', data);
  });
});


/*********************************************************************/
//                                              ↓↓↓ TCPサーバー↓↓↓
/*********************************************************************/
var net = require('net');
var writable = require('fs').createWriteStream('test.txt');

net.createServer(function (socket) {
  console.log('socket connected');
    console.log(socket.remoteAddress);
    
  socket.on('data', function(data) {
    var line = data.toString();
    console.log('got "data"', line);
    socket.pipe(writable);
    io.sockets.emit('emit_from_server', line); // socket.io呼び出し
  });
    
  socket.on('end', function() {
    console.log('end');
  });
  socket.on('close', function() {
    console.log('close');
  });
  socket.on('error', function(e) {
    console.log('error ', e);
  });
  socket.write('hello from tcp server');
}).listen(3080, function() {
  console.log('TCP Server is listening on port 3080');
});